<?php

namespace App\AdminModule\presenters;


use App\AdminModule\components\grids\IArticlesGridFactory;
use App\AdminModule\forms\IArticleFormFactory;
use App\Model\ArticlesRepository;
use Nette\Application\UI\Form;

class ArticlesPresenter extends SecuredPresenter {

	/** @var  IArticlesGridFactory @inject */
	public $articlesGridFactory;

	/** @var  IArticleFormFactory @inject */
	public $articleFormFactory;

	/** @var  ArticlesRepository @inject */
	public $articlesRepository;

	/** @var  \IArticleFormHandlerFactory @inject */
	public $articleFormHandlerFactory;


	protected $article;

	public function actionDefault()
	{
	}

	public function actionDetail($id)
	{
		$article = $this->articlesRepository->get($id);
		$this->article = $article;

		$this->template->article = $article;

		$this['articleForm']->setDefaults($article->toArray());
        
        
        $httpRequest = $this->context->httpRequest;
        $this->template->url =  $httpRequest->getUrl();
        $this->template->id =  $id;
	}


	public function createComponentArticlesGrid()
	{
		$grid = $this->articlesGridFactory->create($this->lang);
		return $grid;
	}


	public function createComponentArticleForm()
	{
		$form = $this->articleFormFactory->create();

		$handler = $this->articleFormHandlerFactory->create($this->lang);
		$handler->setArticle($this->article);

		$form->setHandler($handler);

		return $form;
	}

} 