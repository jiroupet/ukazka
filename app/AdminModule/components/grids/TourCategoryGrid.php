<?php

namespace App\AdminModule\components\grids;


use App\Model\TourCategoryRepository;
use Grido\Components\Filters\Filter;
use Grido\Grid;
use Nette\Application\BadRequestException;

class TourCategoryGrid extends Grid {

	/** @var  TourCategoryRepository */
	protected $tourCategoryRepository;


	protected $version;


	public function __construct($version, TourCategoryRepository $tourCategoryRepository)
	{
		$this->version = $version;

		$this->tourCategoryRepository = $tourCategoryRepository;

		$this->initialize();
	}


	public function initialize()
	{
		$this->getTranslator()->lang = 'cs';
		$this->setFilterRenderType(Filter::RENDER_INNER);
		$this->setModel($this->tourCategoryRepository->findAllByVersion($this->version));

		$this->addColumnText('name', 'Název:')
			->setSortable()
			->setFilterText();

		$this->addActionEvent('delete', 'Smazat', $this->deleteCategory)
			->setIcon('trash-o')
			->setConfirm(function ($item) {
				return "Opravdu chcete smazat kategorii?";
			})
		;

	}


	public function deleteCategory($category)
	{
		$category = $this->tourCategoryRepository->get($category);

		if(!$category)
		{
			throw new BadRequestException("Kategorie neexistuje.");
		}

		$category->delete();

		$presenter = $this->getPresenter();

		$presenter->flashMessage("Kategorie byla odebrána.");
		$presenter->redirect("Tours:categories");
	}

}


interface ITourCategoryGridFactory
{

	/**
	 * @param $version
	 * @return TourCategoryGrid
	 */
	public function create($version);

}