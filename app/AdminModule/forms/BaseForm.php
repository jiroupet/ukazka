<?php

namespace App\AdminModule\forms;


use Kdyby\BootstrapFormRenderer\BootstrapRenderer;
use Kdyby\Replicator\Container;
use Nette\Application\AbortException;
use Nette\Application\UI\Form;
use Nette\Forms\Controls\Button;
use Nette\Forms\Controls\Checkbox;
use Nette\Forms\Controls\CheckboxList;
use Nette\Forms\Controls\MultiSelectBox;
use Nette\Forms\Controls\RadioList;
use Nette\Forms\Controls\SelectBox;
use Nette\Forms\Controls\TextBase;
use Tracy\Debugger;

abstract class BaseForm extends Form {

	/** @var  \IFormHandler  */
	protected $handler;

	/** @var array snippets for invalidation */
	static protected $snippets = [];


	public function __construct($parent = NULL, $name = NULL)
	{
		parent::__construct($parent, $name);

		$this->initialization();

		$this->setupRenderer();
	}

	/** Init form inputs */
	abstract public function initialization();


	public function setHandler(\IFormHandler $handler)
	{
		$this->handler = $handler;
		$this->onSuccess[] = callback($this->handler, 'process');
	}


	public function getHandler()
	{
		return $this->handler;
	}


	protected function setupRenderer()
	{
		// setup form rendering
		$renderer = $this->getRenderer();
		$renderer->wrappers['controls']['container'] = NULL;
		$renderer->wrappers['pair']['container'] = 'div class=form-group';
		$renderer->wrappers['pair']['.error'] = 'has-error';
		$renderer->wrappers['control']['container'] = 'div class=col-sm-10';
		$renderer->wrappers['label']['container'] = 'div class="col-sm-2 control-label"';
		$renderer->wrappers['control']['description'] = 'span class=help-block';
		$renderer->wrappers['control']['errorcontainer'] = 'span class=help-block';

		// make form and controls compatible with Twitter Bootstrap
		$this->getElementPrototype()->class('form-horizontal');
		foreach ($this->getControls() as $control) {
			if ($control instanceof Button) {
				$control->getControlPrototype()->addClass(empty($usedPrimary) ? 'btn btn-primary' : 'btn btn-default');
				$usedPrimary = TRUE;
			} elseif ($control instanceof TextBase || $control instanceof SelectBox || $control instanceof MultiSelectBox) {
				$control->getControlPrototype()->addClass('form-control');
			} elseif ($control instanceof Checkbox || $control instanceof CheckboxList || $control instanceof RadioList) {
				$control->getSeparatorPrototype()->setName('div')->addClass($control->getControlPrototype()->type);
			}
		}
	}


	/**
	 * Snippets invalidation
	 *
	 * @return void
	 */
	public function invalidateSnippets()
	{
		foreach(static::$snippets as $snippet)
		{
			$this->getPresenter()->redrawControl($snippet);
		}
	}

} 