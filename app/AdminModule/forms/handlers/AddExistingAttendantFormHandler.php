<?php

class AddExistingAttendantFormHandler implements IFormHandler {

	/** @var  \App\Model\TourTermPricesRepository */
	protected $tourTermPricesRepository;

	/** @var  \App\Model\AttendanceRepository */
	protected $attendanceRepository;


	public function __construct(\App\Model\TourTermPricesRepository $tourTermPricesRepository, \App\Model\AttendanceRepository $attendanceRepository)
	{
		$this->tourTermPricesRepository = $tourTermPricesRepository;
		$this->attendanceRepository = $attendanceRepository;
	}


	/**
	 * @param \Nette\Application\UI\Form $form
	 * @throws Nette\InvalidStateException
	 * @return mixed
	 */
	function process(\Nette\Application\UI\Form $form)
	{
		$data = $form->getValues(TRUE);

		$attGet = ['tour_term_prices_id', 'additional_price', 'additional_price_note', 'paid', 'visa', 'visa_done', 'insurance', 'insurance_done', 'storno', 'storno_done', 'state'];

		$attendanceData = array_intersect_key($data, array_flip($attGet));
		$attendanceData['attendant_id'] = $data['attendant'];
		$attendanceData['tour_terms_id'] = $this->tourTermPricesRepository->get($attendanceData['tour_term_prices_id'])->ref('tour_terms')->id;

		$attendance = $this->attendanceRepository->create($attendanceData);

		if(!$attendance)
		{
			throw new \Nette\InvalidStateException("Nepodařilo se přidat účastníka k zájezdu.");
		}


		$form->getPresenter(TRUE)->flashMessage('Účastník byl vytvořen a přidán k zájezdu.');
		$form->getPresenter(TRUE)->redirect('this');
	}
}


interface IAddExistingAttendantFormHandlerFactory
{
	/**
	 * @return AddExistingAttendantFormHandler
	 */
	public function create();
}