<?php

namespace App\AdminModule\forms;


class ArticleForm extends BaseForm {

	/** Init form inputs */
	public function initialization()
	{
		$this->addText('title', 'Nadpis:')
			->addRule(self::FILLED, 'Nadpis článku musí být vyplněn.');

		$this->addDatePicker('date_published', 'Publikovat:');

		$this->addTextArea('perex', 'Perex:')
			->addRule(self::FILLED, 'Perex článku musí být vyplněn.');

		$this->addTextArea('text', 'Text:')
			->addRule(self::FILLED, 'Text článku musí být vyplněn.')
			->getControlPrototype()
			->addAttributes([
				'class' => 'text-edit'
			]);

		$this->addUpload('image', 'Obrázek:')
			->addCondition(self::FILLED)
			->addRule(self::IMAGE, 'Nesprávný formát obrázku.');

		$this->addSubmit('send', 'Uložit');
	}
}


interface IArticleFormFactory
{
	/**
	 * @return ArticleForm
	 */
	public function create();
}