<?php

namespace App\Model;


use Nette\Object;

class BaseRepository extends Object {

	/** @var  \Nette\Database\Context*/
	protected $database;

	protected $table;

	const VERSION_COLUMN = 'version';

	public function __construct(\Nette\Database\Context $c)
	{
		$this->database = $c;
	}


	/**
	 * Vrati tabulku pro kazde Repository
	 * @return \Nette\Database\Table\Selection
	 */
	protected function getTable()
	{
		return $this->database->table($this->getTableName());
	}


	public function getTableName()
	{
		if(isset($this->table))
		{
			return $this->table;
		}

		preg_match('#(\w+)Repository$#', get_class($this), $arr);

		return lcfirst($arr[1]);
	}


	/**
	 * Vrati cely selection
	 * @return \Nette\Database\Table\Selection
	 */
	public function findAll()
	{
		return $this->getTable();
	}


	public function findAllByVersion($version)
	{
		return $this->findBy([
			self::VERSION_COLUMN => $version
		]);
	}


	public function get($id)
	{
		return $this->getTable()->get($id);
	}


	public function create($data)
	{
		return $this->getTable()->insert($data);
	}


	/**
	 * Parametrizovany selection
	 *
	 * @param array $arr
	 * @return \Nette\Database\Table\Selection
	 */
	public function findBy(array $arr)
	{
		return $this->getTable()->where($arr);
	}
}


class DoesNotExistException extends \Exception {}